package br.com.basecode.lang.util;

import java.io.File;
import java.math.BigInteger;
import java.text.Normalizer;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

public class StringsUtility {

	private StringsUtility() {}

	public static final char UNDERSCORE = '_';
	public static final String TRACE = "-";
	public static final String BAR = "/";
	public static final String ASTERISCO = "*";
	public static final String POINT = ".";

	/**
	 * Consumer padrão
	 * @return Consumer padrão que não faz nada.
	 */
	public static Consumer<String> doNothing(){
		return s -> {};
	}

	/**
	 * Transforma a String para o Java Case. 
	 * <strong> Ex: </strong>  user_data => userData
	 * 
	 * @param value String em Undescore Case.
	 * @return String em Java Case
	 */
	public static String javaCase(String value) {

		final char[] chars = value.toCharArray();
		final char[] result = new char[ chars.length - StringUtils.countMatches(value, UNDERSCORE) ];

		boolean undescoreBefore = false;

		int j = 0;
		for(int i = 0; i < chars.length; i++) {

			if(chars[i] != UNDERSCORE) {

				result[j] = undescoreBefore ? Character.toUpperCase(chars[i]) : chars[i];
				undescoreBefore = false;
				j++;

			}else {

				undescoreBefore = true;

			}

		}

		return new String(result);

	}


	/**
	 * Transforma a String para o Underscore Case. 
	 * <strong> Ex: </strong> userData | UserData => user_data
	 * 
	 * @param value String em Java Case.
	 * @return String em Underscore Case
	 */
	public static String underscoreCase(String value) {

		if(value != null) {

			final char[] chars = value.toCharArray();

			if(chars.length > 0) {

				StringBuilder result = new StringBuilder();
				result.append(chars[0]);

				char ch;
				for(int i = 1; i < chars.length; i++) {

					ch = chars[i];

					if(Character.isUpperCase(ch)) {
						result.append("_");
					}

					result.append(Character.toLowerCase(ch));

				}

				return result.toString().toLowerCase();
			}

		}

		return null;

	}

	/**
	 * Remove os inícios duplicados em um texto. 
	 * Ex:  
	 * 
	 * <b> Entrada:</b> Eu eu eU amo o Brasil 
	 * <b> Saída:</b> Eu amo o Brasil  
	 * 
	 * @param str Entrada com duplicidades no início
	 * @return Saída sem duplicidades no início.
	 */
	public static String removeDuplicateStart(String str) {

		if(StringUtils.isBlank(str)) return str;

		StringBuilder blder = new StringBuilder();

		List<String> words = Arrays.asList(str.split(StringUtils.SPACE));

		String firstWord = words.get(BigInteger.ZERO.intValue());

		words = words.stream().filter(word -> !word.equalsIgnoreCase(firstWord)).collect(Collectors.toList());

		blder.append(firstWord).append(StringUtils.SPACE).append(String.join(StringUtils.SPACE, words));

		return blder.toString();

	}

	/**
	 * Junta uma ou mais strings com um separador, mas só considera a string se ela não for nula ou vazia.
	 * @param separator saparador, ex: . - /.
	 * @param strs as palavras a serem unidas.
	 * @return Palavras unidas pelo separador.
	 */
	public static String joinIfNotBlank(String separator, String... strs) {

		if(ArrayUtils.isEmpty(strs)) return null;

		Predicate<String> filter = str -> StringUtils.isNotBlank(StringUtils.deleteWhitespace(str));  
		List<String> noBlankList = Arrays.asList(strs).stream().filter(filter).collect(Collectors.toList());

		return StringUtils.join(noBlankList, separator);

	}

	/**
	 * Junta uma ou mais strings,sem separador, mas só considera a string se ela não for nula ou vazia.
	 * @param strs as palavras a serem unidas.
	 * @return Palavras unidas.
	 */
	public static String justJoinIfNotBlank(String... strs) {
		return joinIfNotBlank(null, strs);
	}

	/**
	 * Pega a primeira palavra de uma frase, considerando que um espaço separa as palavras.
	 * @param phrase Frase de onde o texto será extraído.
	 * @return Primeira palavra da frase, ou null, caso a frase seja vazia ou nula.
	 */
	public static String firstWord(String phrase) {
		
		if(StringUtils.isBlank(phrase) || !phrase.contains(StringUtils.SPACE)) return phrase;
		
		return phrase.substring(0, phrase.indexOf(StringUtils.SPACE));

	}

	/**
	 * Verifica se a string contém algum acento. 
	 * Obs: Cedilha é considerado acento.
	 * @param str A palavra a ser verificada.
	 * @return true se houver algum acento na palavra, false caso contrário.
	 */
	public static boolean containsAccents(String str) {
		if(str==null || str.length()==0)
			return false;

		String novaString = Normalizer.normalize(str, Normalizer.Form.NFD);
		return !novaString.equals(str);
	}

	/**
	 * Verifica se a palavra contém caracteres especiais.
	 * @param str A palavra a ser verificada.
	 * @return true se houver algum caractere especial, falso caso contrário.
	 */
	public static boolean containsSpecialChars(String str){
		Pattern p = Pattern.compile("[!#$%&*()_+=|<>?{}\\[\\]~,.]", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(str);
		return m.find();
	}
	
	/**
	 * Remove os caracteres não numéricos de uma String.
	 * @param str String com valor alfanumérico.
	 * @return String somente com caracteres não numéricos.
	 */
	public static String clearNonNumeric(String str) {
		StringBuilder numeric = new StringBuilder();
		if(StringUtils.isNotBlank(str)) {
			char ch;
			for(int i = 0; i < str.length(); i++){
				ch = str.charAt(i);
				if(Character.isDigit(ch)) {
					numeric.append(ch);
				}
			}
			return numeric.toString();
		}
		return str;
	}


	/**
	 * Faz split da String para uma lista.
	 * @param str A string a ser separada.
	 * @param splitChar O caractere utilizado no split.
	 * @param includeEmptyLines Incluir espaços na lista.
	 * @return Split da string numa lista.
	 */
	public static List<String> splitToList(String str, String splitChar, boolean includeEmptyLines){
		if(StringUtils.isNoneBlank(str, splitChar)) {
			if(includeEmptyLines) {
				return Arrays.asList( str.split(splitChar) );
			}
			return Arrays.asList(str.split(splitChar)).stream().filter(StringUtils::isNotBlank).collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	/**
	 * Faz split da String - separada por pontos - para uma lista.
	 * @param str String com pontos separando caracteres/palavras.
	 * @return Lista com as strings que estavam sendo separadas por ponto.
	 */
	public static List<String> splitPoint(String str){
		return splitToList(str, "\\.", false);
	}

	/**
	 * Verifica se a string passada como parâmetro possui, no mínimo, o tamanho informado.
	 * @param str Texto a ser verfificado
	 * @param size Tamanho mínimo  
	 * @return <code> true </code> se a a string tem, pelo menos, o tamanho passado como parâmetro, <code> false </code>, caso contrário.
	 */
	public static boolean checkMinSize(String str, int size) {
		return StringUtils.length(str) >= size;
	}

	/**
	 * <p>
	 * Comprime o texto para um tamanho máximo 
	 * Se o texto tiver um tamanho menor que o máximo, preenche o resto com um caractere informado.
	 * </p>
	 * 
	 * <pre>
	 * 
	 * GipStringUtils.fill('SERPRO',10,'0')='SERPRO0000'
	 * GipStringUtils.fill('SERPRO',4,'0')='SERP'
	 * </pre>
	 * 
	 * @param text O texto a ser comprimido no espaço.
	 * @param size Espaço (tamanho) aonde o texto precisa ser comprimido. (length).
	 * @param fillChar Caractere a ser colocado nos espaços que sobrarem.
	 * @return O Texto com tamanho ajustado.
	 */
	public static String fill(String text, int size, char fillChar) {
		if(StringUtils.isNotBlank(text)) {
			if(text.length() > size) {
				return text.substring(0, size);
			}else if(text.length() < size) {
				return StringUtils.rightPad(text, size, fillChar);
			}
		}
		return text;
	}

	
	/**
	 * <p>
	 * Comprime o texto para um tamanho máximo 
	 * Se o texto tiver um tamanho menor que o máximo, preenche Á esquerda com um caractere informado.
	 * </p>
	 * 
	 * <pre>
	 * 
	 * GipStringUtils.fillLeft('SERPRO',10,'0')='0000SERPRO'
	 * </pre>
	 * 
	 * @param text O texto a ser comprimido no espaço.
	 * @param size Espaço (tamanho) aonde o texto precisa ser comprimido. (length).
	 * @param fillChar Caractere a ser colocado nos espaços, à esquerda, que sobrarem.
	 * @return O Texto com tamanho ajustado.
	 */
	public static String fillLeft(String text, int size, char fillChar) {
		if(StringUtils.isNotBlank(text)) {
			if(text.length() > size) {
				return text.substring(0, size);
			}else if(text.length() < size) {
				return StringUtils.leftPad(text, size, fillChar);
			}
		}
		return text;
	}

	/**
	 * <p>
	 * Comprime o texto para um tamanho máximo 
	 * Se o texto tiver um tamanho menor que o máximo, preenche o resto com espaços.
	 * </p>
	 * 
	 * <pre>
	 * 
	 * GipStringUtils.fill('SERPRO',10,'0')='SERPRO    '
	 * </pre>
	 * 
	 * @param text O texto a ser comprimido no espaço.
	 * @param size Espaço (tamanho) aonde o texto precisa ser comprimido. (length).
	 * @return O Texto com tamanho ajustado.
	 */
	public static String fill(String text, int size) {
		return fill(text, size, StringUtils.SPACE.charAt(0));
	}

	/**
	 * Filtra, numa lista, os valores que começam com o parâmetro informado e retorna o primeiro valor encontrado. 
	 * Ex: 
	 * filterByStart (bra, {Brasil, Brasileiro, Polonês, Americano, Branco}) => Brasil  
	 * 
	 * @param start início da palavra
	 * @param list lista que contém as palavras.
	 * @return O primeiro valor encontrado com começando com o parâmetro informado.
	 */
	public static String filterByStart(String start, List<String> list) {
		if(CollectionUtils.isNotEmpty(list) && StringUtils.isNotBlank(start)) {
			return list.stream().filter(str -> str.toLowerCase().startsWith(start.toLowerCase())).findFirst().orElse(null);
		}
		return null;
	}

	/**
	 * Checa se a string é um asterisco (*).
	 * @param str A string a ser checada.
	 * @return true se for um asterisco, false caso contrário.
	 */
	public static boolean isAsterisc(String str) {
		return str != null && ASTERISCO.equals(StringUtils.trimToEmpty(str));
	}

	/**
	 * Checa se a string NÃO é um asterisco(*).
	 * @param str A string a ser checada.
	 * @return true se NÃO for um asterisco, false caso contrário.
	 */
	public static boolean isNotAsterisc(String str) {
		return !isAsterisc(str);
	}

	/**
	 * Transforma uma string numérica com um separador numa lista de inteiros.
	 * @param str String numérica com um separador, tipo "1,3,5,10".
	 * @param separator O separador da string, como ",|."
	 * @return Lista de inteiros.
	 */
	public static List<Integer> toIntList(String str, String separator){
		if(StringUtils.isNoneBlank(str, separator) && str.contains(separator)) {
			return Arrays.asList(str.split(separator)).stream().map(Integer::valueOf).collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	/**
	 * Transforma uma string numérica com um separador numa lista de Long.
	 * @param str String numérica com um separador, tipo "1,3,5,10".
	 * @param separator O separador da string, como ",|."
	 * @return Lista de Long.
	 */
	public static List<Long> toLongList(String str, String separator){
		if(StringUtils.isNoneBlank(str, separator) && str.contains(separator)) {
			return Arrays.asList(str.split(separator)).stream().map(Long::valueOf).collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	public static long countNonBlank(String... data) {
		try {
			return Arrays.asList(data).stream().filter(StringUtils::isNotBlank).count();
		}catch(Exception e) {/**/}
		return BigInteger.ZERO.longValue();
	}

	/**
	 * Retorna o String.valueOf do objeto, ou Empty, caso o objeto seja nulo.
	 * @param obj O Objeto.
	 * @return O String.valueOf do objeto ou Empty, se o objeto é nulo.
	 */
	public static String orEmpty(Object obj) {
		if(obj != null) {
			return String.valueOf(obj);
		}
		return StringUtils.EMPTY;
	}

	/**
	 * Retorna o String.valueOf do objeto, ou null, caso o objeto seja nulo.
	 * @param obj O Objeto.
	 * @return O String.valueOf do objeto ou null, se o objeto é nulo.
	 */
	public static String orNull(Object obj) {
		if(obj != null) {
			String strValue = String.valueOf(obj);
			if(StringUtils.isNotBlank(strValue.trim())) {
				return strValue;
			}
		}
		return null;
	}

	public static String lastPathName(String path) {
		if(StringUtils.isBlank(path)) return path;
		return path.substring(path.lastIndexOf(File.separator) + 1);
	}


}