package br.com.basecode.lang.util;

import java.math.BigInteger;
import java.text.MessageFormat;

import org.apache.commons.lang3.Range;
import org.apache.commons.lang3.math.NumberUtils;



public class NumbersUtility {

	private NumbersUtility() {}
	
	private static final String WRONG_RANGE_MSG = "wrong range ({0},{1})";
	
	public static final int ZERO = BigInteger.ZERO.intValue();
	
	public static final int TWO = 2;
	
	public static boolean isNotZero(int val, boolean negativeAllowed) {
		
		if(negativeAllowed) {
			
			return val != BigInteger.ZERO.intValue();
			
		}
		
		return val > BigInteger.ZERO.intValue();
	}
	
	public static boolean isNotZero(int val) {
		
		return isNotZero(val, false);
		
	}
	
	/**
	 * Informa se um número é pelo menos igual, ou maior que outro. 
	 * @param a O Número fixo (Base para o cálculo)
	 * @param b O número que precisa ser pelo menos igual ao número fixo.
	 * @return <code>true</> se b é pelo menos igual, ou maior a, <code>false</code>, caso contrário.
	 */
	public static boolean isAtLeast(int a, int b) {
		return b >= a;
	}
	
	/**
	 * Informa se um número possui pelo menos o mínimo de um Range. 
	 * 
	 * @param O Range a ser utilizado na comparação.
	 * @param val O valor que precisa ter no mínimo, o tamanho mínimo do range.
	 * @return <code>true</code> se o valor tiver no mínimo, o tamanho mínimo do range, <code>false</code>, caso contrário.
	 */
	public static boolean hasMinimalSize(Range<Integer> range, Integer val) {

		if(range != null && val != null) {
			
			return val >= range.getMinimum();
			
		}
		
		return false;
		
	}
	
	public static boolean isPositive(Number number) { 
		return number != null && number.doubleValue() > NumberUtils.DOUBLE_ZERO;
	}
	
	public static void checkRange(int start, int end) {
		if(start > end || start < 0 || end < 0) {
			throw new IllegalArgumentException(MessageFormat.format(WRONG_RANGE_MSG, String.valueOf(start), String.valueOf(end)));
		}
	}
	
	public static boolean isValidRange(int start, int end) {

		try {
			checkRange(start, end);
			return true;
		}catch(Exception e) {
			return false;
		}

	}
}
