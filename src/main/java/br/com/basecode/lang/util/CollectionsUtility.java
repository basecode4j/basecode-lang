package br.com.basecode.lang.util;

import java.math.BigInteger;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import br.com.basecode.lang.GroupedList;
import br.com.basecode.lang.Wrapped;



public class CollectionsUtility {

	private CollectionsUtility() {}

	/**
	 * Retorna, de uma lista qualquer, o elemento que está em determinado índice., 
	 * @param list A lista utilizada.
	 * @param index O índice do elemento.
	 * @return O Elemento que está no índice, caso ele exista ou nulo, se o índice não existe.
	 */
	public static <X> X get(List<X> list, int index) {
		if(CollectionUtils.size(list) > index) {
			return list.get(index);
		}
		return null;
	}

	/**
	 * Verifica se todos os elementos da lista são NÃO NULOS. 
	 * Note que, se for passada uma Array nula. o método retorna false.
	 * @param objs Array de objetos.
	 * @return true, caso a lista possua elementos e nenhum elemento seja nulo, false caso contrário.
	 */
	public static boolean isNotNull(Object... objs) {
		if(ArrayUtils.isEmpty(objs)) return false;
		for (int i = 0; i < objs.length; i++) {
			if(objs[i] == null) {
				return false;
			}
		}
		return true;
	}


	/**
	 * Agrupa objetos a partir de um atributo comum.
	 * @param list Lista desagrupada
	 * @param spec Função para se obter o valor do atributo comum.
	 * @return Mapa cuja chave é o valor do atributo comum e o valor é a lista de objetos
	 * que possuem esse atributo com mesmo valor.
	 */
	public static <X,Y> Map<X, List<Y>> group(List<Y> list, Function<Y,X> spec){

		final Map<X,List<Y>> mappedList = new HashMap<>();

		if(CollectionUtils.isNotEmpty(list) && spec != null) {

			Set<X> keySet = list.stream().map(spec::apply).filter(Objects::nonNull).collect(Collectors.toSet());

			keySet.forEach(key -> mappedList.put(key, getMatchedList(list, spec, key)));

		}

		return  mappedList;

	}

	/**
	 * Agrupa objetos a partir de um atributo commum, trazendo como valor o primeiro resultado encontrado.
	 * @param list Lista a ser agrupada.
	 * @param Função para se obter o valor do atributo comum.
	 * @return Mapa cuja chave é o valor do atributo comum e o valor é o primeiro resultado da lista de objetos.
	 */
	public static <X,Y> Map<X, Y> mapFirst(List<Y> list, Function<Y,X> spec){

		final Map<X,Y> mappedList = new HashMap<>();

		if(CollectionUtils.isNotEmpty(list) && spec != null) {

			Set<X> keySet = list.stream().map(spec::apply).filter(CollectionsUtility::isNotNull).collect(Collectors.toSet());

			keySet.forEach(key -> {

				List<Y> matchedList = getMatchedList(list, spec, key);

				mappedList.put(key, matchedList.get(BigInteger.ZERO.intValue()));

			});

		}

		return  mappedList;

	}

	/**
	 * Em uma Lista de X, agrupa os objetos por Y e coloca dentro de um DataGroup de Y,X.
	 * @param list A lista a ser agrupada.
	 * @param spec Função agrupadora.
	 * @param clazz DataGroup para o resultado.
	 * @return Uma lista de DataGroup, conforme a função agrupadora.
	 */
	public static <X,Y,D extends GroupedList<X,Y>> List<D> group(List<Y> list, Function<Y, X> spec, Class<D> clazz){

		List<D> groupedList = new ArrayList<>();

		Map<X, List<Y>> groupedMap = group(list, spec);

		List<Y> yList;

		X x;
		for(Entry<X, List<Y>> entry:groupedMap.entrySet()) {
			x = entry.getKey();
			yList = entry.getValue();
			try {
				groupedList.add(clazz.getDeclaredConstructor(x.getClass(), List.class).newInstance(x,yList));
			} catch (Exception e) {/**/}
		}

		return groupedList;

	}

	/**
	 * Agrupa objetos a partir de um atributo comum e retorna apenas os valores.
	 * @param list Lista desagrupada
	 * @param spec Função para se obter o valor do tributo comum.
	 * @return Coleção de listas, aonde cada lista é o conjunto de elementos agrupados pelo atributo comum.
	 */
	public static <X,Y> Collection<List<Y>> groupValues(List<Y> list, Function<Y,X> spec){

		Map<X, List<Y>> groupedList = group(list, spec);

		return groupedList.values();

	}


	/**
	 * A partir de uma Lista de Y e de uma chave X, retorna todos os itens que,
	 * filtrados, resultem em f(Y) = X.
	 * @param list A lista a ser filtrada.
	 * @param spec A função utilizada para filtrar,i.E, f(Y).
	 * @param key A chave a ser comparada com o resultado da função f(Y)
	 * @return Todos os elementos que resultem em f(Y) = X, isto é o parâmetro key.
	 */
	public static <X,Y> List<Y> getMatchedList(List<Y> list, Function<Y,X> spec, X key){

		Predicate<Y> filter = y -> Objects.equals(key, spec.apply(y));

		return list.stream().filter(filter).collect(Collectors.toList());

	}


	/**
	 * Reduz as listas de uma coleção de entradas, para uma lista só, ignorando as chaves.
	 * @param entries Lista de Entradas.
	 * @return Lista única com todos os valores das listas de todas as entradas.
	 */
	public static <X,Y> List<Y> reduce(List<SimpleEntry<X, List<Y>>> entries){

		List<Y> yList = new ArrayList<>();

		if(CollectionUtils.isNotEmpty(entries)) {

			List<Y> value;
			for(int i = 0; i<entries.size(); i++) {
				value = entries.get(i).getValue();
				if(value != null) {
					yList.addAll(value);
				}
			}

		}

		return yList;

	}

	/**
	 * Coleta todas as chaves da lista de entradas e coloca numa lista.
	 * @param entries Lista de Entradas.
	 * @return Lista com as chaves de todas as entradas.
	 */
	public static <X,Y> List<X> collectKeys(List<SimpleEntry<X,Y>> entries){

		List<X> xList = new ArrayList<>();

		if(CollectionUtils.isNotEmpty(entries)) {

			for(int i = 0; i<entries.size(); i++) {
				xList.add(entries.get(i).getKey());
			}

		}

		return xList;

	}

	/**
	 * Coleta todos os valores das entradas e coloca numa lista.
	 * @param entries Lista de Entradas.
	 * @return Lista com os valores de todas as entradas.
	 */
	public static <X,Y> List<Y> collectValues(List<SimpleEntry<X,Y>> entries){

		List<Y> xList = new ArrayList<>();

		if(CollectionUtils.isNotEmpty(entries)) {

			for(int i = 0; i<entries.size(); i++) {
				xList.add(entries.get(i).getValue());
			}

		}

		return xList;

	}

	/**
	 * Retorna um subconjunto da lista somente com valores não nulos.
	 * @param xList A lista principal.
	 * @return Lista somente com valores não nulos.
	 */
	public static <X> List<X> clearNulls(List<X> xList){

		if(CollectionUtils.isNotEmpty(xList)) {

			return xList.stream().filter(Objects::nonNull).collect(Collectors.toList());

		}

		return xList;

	}

	/**
	 * Retorna uma sublista, a partir de uma lista com, no máximo, a quantidade de itens
	 * passada como parâmetro.
	 * @param list A lista original.
	 * @param maxSize O tamanho máximo de itens que podem ser retornados.
	 * @return A sublista, quando o máximo informado é menor que o tamanho da lista.
	 *         A própria lista quando o máximo informado for menor ou igual a zero ou, ainda, quando o tamanho máximo for maior que 
	 *         o tamanho da lista original.
	 */
	public static <X> List<X> getSubList(List<X> list, int maxSize){

		if(CollectionUtils.isEmpty(list)) return list;

		return list.subList(0, Integer.min(list.size(), maxSize));

	}

	/**
	 * Retorna a soma dos tamanhos das listas de um mapa de listas.
	 * @param listMap O Mapa de listas.
	 * @return Soma dos tamanhos de todas as litas.
	 */
	public static <X,Y> int count(Map<X,List<Y>> listMap) {

		if(MapUtils.isNotEmpty(listMap)) {

			return listMap.values().stream().mapToInt(List::size).sum();

		}

		return NumberUtils.INTEGER_ZERO;

	}

	/**
	 * Extrai os valores do mapa e coloca em uma lista única.
	 * @param listMap Mapa com listas.
	 * @return Todos os valores das listas do Map em uma lista só.
	 */
	public static <X,Y> List<X> extractValues(Map<Y,List<X>> listMap){

		if(MapUtils.isNotEmpty(listMap)) {
			List<X> xList = new ArrayList<>();
			listMap.forEach((k,v) -> {
				if(v!=null) {
					xList.addAll(v);
				}
			});
			return xList;
		}

		return Collections.emptyList();

	}


	/**
	 * Retorna uma lista de <code>Long</code> a partir de uma lista de caracteres numéricos. 
	 * Este método permite informar uma sublista.
	 * @param args Uma lista de caracteres.
	 * @param start início da sublista
	 * @param end fim da sublista
	 * @return uma lista de Long.
	 */
	public static List<Long> asLongList(String[] args, int start, int end){

		NumbersUtility.checkRange(start, end);

		String[] subArray = ArrayUtils.subarray(args, start, Integer.min(args.length, end));

		List<Long> resultList = new ArrayList<>();

		for(String data:subArray) {
			try {
				resultList.add(Long.parseLong(data));
			}catch(Exception e) {/**/}
		}

		return resultList;

	}

	/**
	 * Retorna um "Consumer Adicionador de Lista". 
	 * Utilizando uma função (y) -> x, o consumer de y adiciona o resultado da operação em uma lista de x.
	 *  
	 * @param xList A lista a ser incrementada. 
	 * @param fn Função (y) -> x, i.E, que transforma y em x.
	 * @return Consumer que incrementa a lista com o resultado da função informada.
	 */
	public static <X,Y> Consumer<Y> getAdder (List<X> xList, Function<Y, X> fn){
		return y -> xList.add( fn.apply(y) );
	}

	/**
	 * Retorna um "BIConsumer Adicionador de Lista". 
	 * Utilizando uma função (y,z) -> x, o consumer de (y,z) adiciona o resultado da operação em uma lista de x.
	 *  
	 * @param xList A lista a ser incrementada. 
	 * @param fn Função (y,z) -> x, i.E, que transforma (y,z) em x.
	 * @return BiConsumer que incrementa a lista com o resultado da função informada.
	 */
	public static <X,Y,Z> BiConsumer<Y,Z> getAdder(List<X> xList, BiFunction<Y,Z,X> fn){
		return ( y, z ) -> xList.add( fn.apply(y, z) );
	}

	/**
	 * Procura na lista o primeiro a partir da função o retorna.
	 * @param data Lista base
	 * @param fn Função de pesquisa.
	 * @return Primeiro objeto encontrado na lista pela função.
	 */
	public static <X,Y> X findFirst(List<Y> data, Function<Y,X> fn) {

		final Wrapped<X> obj = new Wrapped<>();

		data.stream().map(fn).findAny().ifPresent(obj::set);	

		return obj.get();			

	}	

	/**
	 * Transforma uma coleção em valores separados por vírgula. 
	 * Este método utiliza o String.valueOf nos objetos da lista.
	 * 
	 * @param list lista de valores.
	 * @return Valores da lista separados por vírgula.
	 */
	public static final String toString(Collection<?> list) {

		if(CollectionUtils.isNotEmpty(list)) {

			return list.stream().map(String::valueOf).reduce((a,b) -> String.join(",", a,b)).get();

		}

		return StringUtils.EMPTY;

	}

	public static <E> E getFirst(List<E> list) { 
		if(CollectionUtils.isEmpty(list)) return null;
		return list.get(BigInteger.ZERO.intValue());
	}

}