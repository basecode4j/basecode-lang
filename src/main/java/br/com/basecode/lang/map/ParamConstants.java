package br.com.basecode.lang.map;

import java.util.Arrays;
import java.util.List;

public class ParamConstants {
	
	private ParamConstants() {/**/}
	
	protected static final List<String> BOOLEANS = Arrays.asList("1","v","s","t","sim");

	protected static final String NUMERICS = "0123456789";

}
