package br.com.basecode.lang.map;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;

public class ParamMap {
	
	private final Map<String,Param> map = new HashMap<>();
	
	public ParamMap(Collection<? extends Param> params) {
		if(CollectionUtils.isNotEmpty(params)) {
			params.forEach(this::put);
		}
	} 
	
	public void put(Param param) {
		this.map.put(param.getName(), param);
	}
	
	public Param get(String id) {
		return this.map.get(id);
	}
	
	public boolean contains(String key) {
		return this.map.containsKey(key);
	}
	
	public Param get(Object id) {
		return this.map.get(String.valueOf(id));
	}

	public Param getSafe(String id) {
		Param param = get(id);
		return param != null ? param : Param.safe(id);
	}

	public Param getSafe(Object obj) {
		Param param = get(obj);
		return param != null ? param : Param.safe(String.valueOf(obj));
	}
	
	public static boolean check(ParamMap map, String... keys) {
		if(map != null && ArrayUtils.isNotEmpty(keys)) {
			for(String key:keys) {
				if(!map.map.containsKey(key)) {
					return false;
				}
			}
			return true;
		}else {
			return false;
		}
	}
	
	public static String[] getMissingKeys(ParamMap map, String... keys) {
		if(map != null && ArrayUtils.isNotEmpty(keys)) {
			List<String> missingList = new ArrayList<>();
			for(String key:keys) {
				if(!map.map.containsKey(key)) {
					missingList.add(key);
				}
			}
			return missingList.toArray(new String[] {});
		}
		return new String[] {}; 
		
	}
	
	/**
	 * Junta os parâmetros do mapa atual e do que foi fornecido como parâmetro, 
	 * retornando uma nova instância de {@link ParamMap}
	 * @param paramMap Mapa de parâmetros a ser unido com a instância atual.
	 * @return nova instância de {@link ParamMap}
	 */
	public ParamMap join(ParamMap paramMap) {
		Collection<Param> params = new ArrayList<>();
		params.addAll(this.map.values());
		if(paramMap != null) {
			params.addAll(paramMap.map.values());
		}
		return new ParamMap(params);
	}
	

	public boolean isNotEmpty() {
		return this.map.size() > 0;
	}
	
	public static boolean isNotEmpty(ParamMap map) {
		return map != null && !map.map.isEmpty();
	}
	
	public static ParamMap empty() {
		return new ParamMap(Collections.emptyList());
	}
	
	public Map<String,Param> getRawMap(){
		return this.map;
	}
}