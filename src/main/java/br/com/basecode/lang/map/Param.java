package br.com.basecode.lang.map;

import static br.com.basecode.lang.map.ParamConstants.BOOLEANS;
import static br.com.basecode.lang.map.ParamConstants.NUMERICS;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;

import br.com.basecode.lang.util.StringsUtility;



public interface Param extends Serializable{

	String getName();
	
	String getValue();

	default Boolean asBoolean() {
		if(BOOLEANS.contains(asString().toLowerCase())) {
			return true;
		}
		return Boolean.valueOf(asString());
	}

	default boolean isEmpty() {
		return StringUtils.isBlank(getValue());
	}

	default boolean isNotEmpty() {
		return !isEmpty();
	}

	default List<Integer> asIntList(String separator){

		return StringsUtility.toIntList(getValue(), separator);

	}

	default List<Long> asLongList(String separator){

		return StringsUtility.toLongList(getValue(), separator);

	}

	public default Long asLongOrZero() {
		Long asLong = this.asLong();
		return asLong != null ? asLong : 0L;
	}

	
	default List<String> asStringList(String separator){
		return StringsUtility.splitToList(getValue(), separator, false);
	}  

	/**
	 * Retorna o valor como String
	 * @return se nulo, retorna vazio, senão, retorna <code>getValue</code>
	 */
	default String asString() {
		return StringUtils.defaultIfBlank(getValue(), StringUtils.EMPTY).trim();
	}
	
	public default <T> T as(Function<String, T> fn){ 
		return fn.apply(getValue());
	}


	default String[] asArray() {
		return asString().split(StringUtils.SPACE);
	}

	default Long asLong() {
		try {
			return Long.parseLong(asString());
		}catch(Exception e) {
			return null;
		}
	}

	default Integer asInteger() {
		try {
			return Integer.parseInt(asString());
		}catch(Exception e) {
			return null;
		}
	}

	default Double asDouble() {
		try {
			return Double.parseDouble(asString());
		}catch(Exception e) {
			return null;
		}
	}

	default BigDecimal asBigDecimal() {
		try {
			return BigDecimal.valueOf(this.asDouble());
		}catch(Exception e) {
			return null;
		}
	}

	default Date asDate(String format) {
		try {
			return new SimpleDateFormat(format).parse(asString());
		}catch(Exception e) {
			return null;
		}
	}

	default boolean is(String nome) {
		return StringUtils.isNotBlank(getName()) && getName().equals(nome);
	}

	default boolean eq(Integer val) {
		return val != null && val.equals(asInteger());
	}

	default boolean eq(Double val) {
		return val != null && val.equals(asDouble());
	}

	default boolean eq(Boolean val) {
		return val != null && val.equals(asBoolean());
	}

	default boolean eq(String val) {
		return val != null && val.equalsIgnoreCase(asString());
	}

	default boolean isTrue() {
		return Boolean.TRUE.equals(this.asBoolean());
	}

	default boolean isFalse() {
		return !this.isTrue();
	}

	default boolean isNotTrue() {
		return isFalse();
	}

	default BigDecimal asPercent() {
		BigDecimal value = asBigDecimal();
		if(value == null) {
			return new BigDecimal(0);
		}
		return value.divide(BigDecimal.valueOf(100), 3, RoundingMode.FLOOR);
	}

	default String format(Object ...data) {
		return MessageFormat.format(this.asString(), data);
	}

	default boolean isDigit() {
		if(!isEmpty()) {
			return NUMERICS.indexOf(getValue()) != -1;
		}
		return false;
	}
	
	static Param safe() {
		return safe(null);
	}
	
	static Param safe(String name) {
		return new Param() {
			private static final long serialVersionUID = 1L;
			@Override
			public String getValue() {
				return null;
			}
			@Override
			public String getName() {
				return name;
			}
			@Override
			public boolean isMutable() {
				return false;
			}
		};
	}

	static boolean asBooleanOrTrue(Param param) {
		if(param == null) return true;
		return param.asBoolean();
	}

	static boolean asBooleanOrFalse(Param param) {
		if(param == null) return false;
		return param.asBoolean();
	}

	static boolean isTrue(Param param) {
		return param != null && param.isTrue();
	}

	static boolean isNotTrue(Param param) {
		return param == null || param.isNotTrue();
	}

	static boolean eq(Param param, Integer val) {

		if(param != null && val != null) {
			return val.equals(param.asInteger());
		}

		return false;

	}

	default boolean isMutable() {
		return true;
	}
	
}