package br.com.basecode.lang.map;

import java.util.Objects;

public class SimpleParam implements Param{

	private static final long serialVersionUID = 1L;

	private String name;
	private String value;
	
	public SimpleParam(String name, String value) {
		this.name = name;
		this.value = value;
	}
	
	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getValue() {
		return this.value;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleParam other = (SimpleParam) obj;
		return Objects.equals(name, other.name);
	}
	
}