package br.com.basecode.lang.map;

import java.util.Objects;

public class SystemParam implements Param{

	private static final long serialVersionUID = 1L;

	private final String name;
	
	public SystemParam(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getValue() {
		return System.getProperty(name);
	}
	
	
	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SystemParam other = (SystemParam) obj;
		return Objects.equals(name, other.name);
	}
}
