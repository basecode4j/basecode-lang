package br.com.basecode.lang;

public class Wrapped <X>{
	
	private X object;
	
	public Wrapped(X x) {
		this.object = x;
	}
	
	public Wrapped() {}
	
	public void set(X x) {
		this.object = x;
	}

	public X get() {
		return object;
	}
	
	public boolean isNotNull() {
		return object != null;
	}
	
	public Wrapped<X> setThen(X x){
		set(x);
		return this;
	}
	
}
