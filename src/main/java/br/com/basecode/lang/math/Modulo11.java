package br.com.basecode.lang.math;



public class Modulo11{

	public Modulo11() {/**/}
	
	public int calcular(String key) {
        int total = 0;    
        int peso = 2;    
                
        for (int i = 0; i < key.length(); i++) {    
            total += (key.charAt((key.length()-1) - i) - '0') * peso;    
            peso ++;    
            if (peso == 10)    
                peso = 2;    
        }    
        int resto = total % 11;    
        return (resto == 0 || resto == 1) ? 0 : (11 - resto);
	}

}
