package br.com.basecode.lang;

import java.util.List;

public class GroupedList<X, Y> {

	private final X by;
	
	private final List<Y> list;
	
	public GroupedList(X by, List<Y> list) {
		this.by = by;
		this.list = list;
	}
	
	
	public X getBy() {
		return by;
	}
	
	public List<Y> getList() {
		return list;
	}
	
}
